# mutaciones


## Clonacion del repositorio 

Para poder clonar el repositorio ingrese a un directorio y ejecute

```
git clone https://gitlab.com/daviddelgado513/mutaciones.git
```

## Instalacion de depencias
Este proyecto se ejecuta a travez de docker-compose y fue hecho utilizando laravel sails

Para instalar instalar las dependencias ingrese al directorio src y ejecute

```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
```
Configure un alias para sails, facilitando la ejecucion de los siguientes comandos
```
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
```


Esto creara un contenedor temporal con las dependencias de PHP necesarias
para ejecutar composer.

```
A continuacion copie el archivo .env que se envio a traves del email en el directorio .src .
```

Ejecute las migraciones de laravel con el comando de artisan a travez de sails

```
sail artisan migrate
```

## Nota: Asegurece de tomar en cuenta que su usuario tenga acceso al grupo docker

## Ejecucion del servicio
A travez de docker ejecute el comando de docker-compose para levantar los
servicios requeridos

```
docker-compose up
```
o
```
docker-compose up -d
```

## ejecucion de las pruebas unitarias
A traves de sails ejecute el comando de artisan para correr las pruebas unitarias

```
sail artisan tests
```

## acceso a los endpoints
Antes de ejecutar un endpoint (/mutation y /stats) tendra que obtener un token
de autentificacion de jwt con el endpoint /login

```
POST /login
PARAMS (JSON):
{
    "email": "guros@example.com",
    "password": "passr"
}
```

Para facilitar la prueba se adjunta el proyecto una collecion de Postman
```
Guros.postman_collection.json
```

La consulta a /login le arrojara un token jwt en el campo access_token que debera adjuntar
en el header 'Bearer' antes de poder consultar a los /endpoints

