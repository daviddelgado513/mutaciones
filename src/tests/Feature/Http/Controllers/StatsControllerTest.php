<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StatsControllerTest extends TestCase
{
    /**
     * Conseguir el token JWT
     */
    protected function setUp(): void
    {
        parent::setUp();
        
        // obtener el token
        $res = $this->post('/login',[
            'email' => env('EMAIL'),
            'password' => env('PASSWORD')
        ]);

        $this->token = $res->decodeResponseJson()['access_token'];
    }
    
    
    /**
     * Comprobar autentificacion del endpoint 
     *
     * @return void
     */
    public function testRouteAuth()
    {
        $this->get('/stats')->assertForbidden();
    }

}
