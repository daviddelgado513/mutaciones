<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MutationControllerTest extends TestCase
{
    private $token;

    protected function setUp(): void
    {
        parent::setUp();
        
        // obtener el token
        $res = $this->post('/login',[
            'email' => env('EMAIL'),
            'password' => env('PASSWORD')
        ]);

        $this->token = $res->decodeResponseJson()['access_token'];
    }
    
    /**
     * Comprobar que la ruta esta protegida
     */
    public function testRouteNoAuth()
    {
        $res = $this->post('/mutation');

        $this->assertEquals(403,$res->status(),'la ruta no esta protegida');

    }
    
    /**
     * Comprobar que con token la ruta este disponible
     */
    public function testRouteAuth()
    {
        $res = $this->withToken($this->token)->post('/mutation');

        echo $res->status();

        $this->assertNotEquals(404,$res->status());           
    }


    /**
     * Comprobar el comportamiento de la API
     * POST /mutation
     * @return void
     */
    public function testPostSuccessMutation()
    {
        // Caso A True
        $matrizA = ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"];        


        $response = $this->withToken($this->token)->json('POST','/mutation',[
            'adn' => $matrizA
        ]);

        $flag = $response->status();

        $this->assertEquals(200,$flag,$response->content());

    }

    public function testPostFailMutation()
    {
        $matrizB = ["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"];


        $response = $this->withToken($this->token)->json('POST','/mutation',[
            'adn' => $matrizB
        ]);

        $flag = $response->status();

        $this->assertEquals(403,$flag,$response->content());        
    }


}
