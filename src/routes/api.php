<?php

use App\Http\Controllers\MutationController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('login',[UserController::class,'authenticate']);



Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('/mutation',MutationController::class);

    Route::get('/stats',StatsController::class);

});