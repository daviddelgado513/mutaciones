<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // forzar el dato adn y tomar un tamaño de 255 maximo eq a 15 x 15 en la matriz
        Schema::create('genomas', function (Blueprint $table) {
            $table->id();
            $table->string('adn',255);
            $table->integer('length');
            $table->boolean('mutado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genomas');
    }
}
