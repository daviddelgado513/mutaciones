<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * 
     * Valida que el tamaño de la matriz DNA sea de NxN
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        $flag = true;
        $len = count($value);

        foreach ($value as $linea ) {
            if ($len != strlen($linea)) {
                $flag = false;
                break;
            }
        }

        return $flag;        
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'No es un adn valido, NxN.';
    }
}
