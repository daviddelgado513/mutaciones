<?php

namespace App\Http\Controllers;

use App\Repositories\GenomaRepositoryInterface;
use Illuminate\Http\Request;

class StatsController extends Controller
{
    private $repositorio;

    public function __construct(GenomaRepositoryInterface $repository)
    {
        $this->repositorio = $repository;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        //
        $res = $this->repositorio->stats();

        return response($res);
    }
}
