<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
      $credentials = $request->only('email', 'password');
      try {
          if (! $token = JWTAuth::attempt($credentials)) {
              return response()->json(['error' => 'Acceso denegado'], 403);
          }
      } catch (JWTException $e) {
          return response()->json(['error' => 'Error al crear el token'], 403);
      }
      
      return response()->json([
        'access_token' => $token,
        'token_type' => 'bearer'
        ]);
    }    
}
