<?php

namespace App\Http\Controllers;

use App\Http\Requests\MutationRequest;
use App\Http\Resources\GenomaResource;
use App\Models\Genoma;
use App\Repositories\GenomaRepositoryInterface;
use Illuminate\Http\Request;

class MutationController extends Controller
{
    private $repositorio;

    public function __construct(GenomaRepositoryInterface $repository)
    {
        $this->repositorio = $repository;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(MutationRequest $request)
    {
        
        // Validar
        $data = $request->validated();

        // realizar comparativa
        $conteoMutacion = $this->checkForMutation($data['dna']);

        // si cumple con los criterios almacenar en la base de datos

        if ($conteoMutacion > 1) {
            $data['mutado'] = true;

            $this->repositorio->create($data);

            return response('Ok',200);
        }
        else{
            $data['mutado'] = false;

            $this->repositorio->create($data);
            
            return response('Error',403);
        }
    }
    /**
     * Compruba el adn de una matriz NxN para encontrar 
     * coincidencias de 4 caracteres en columnas filas y diagonales
     * @param array[][] $dna
     * @return int $conteo
     */
    private function checkForMutation($dna){
        $conteo = 0;
        
        $length = count($dna);


        $controlY = 0;
        // control de coincidencias en el eje X array de N posiciones por cada linea
        $controlX = array_pad(array(),$length,0);
        // FIFO de diagonal positiva ( | )
        $diagonalPositiva = array_pad(array(),$length,0);
        // FIFO de diagonal negativa (/)
        $diagonalNegativa = array_pad(array(),$length,0);

        // el control X se refiere a las lineas (string)
        // el control Y se refiere a las columnas (array items)

        for ($x=0; $x < $length; $x++) { 
            
            // conteo de coincidencias en el eje Y
            $controlY = 0;
            for ($y=0; $y < $length; $y++) { 
                // revisar por columna
                if (($y + 1) < $length) {
                    if ($dna[$x][$y] == $dna[$x][$y + 1]) {
                        $controlY +=  1;
                    }
                    else{
                        $controlY = intdiv($controlY,4) * 4;
                    }
                }

                if ($controlY >= 3) {
                    $conteo += intdiv($controlY,3);
                }

                // revisar lineas guardando referencias en array de N posiciones

                if (($x + 1) < $length) {
                    if ($dna[$x][$y] == $dna[$x + 1][$y]) {
                        $controlX[$y] += 1;
                    }
                    else{
                        $controlX[$y] = intdiv($controlX[$y],3) * 3;
                    }
                }

                // en el caso de las diagonales el recorrido es linea por linea
                // se invierten los indices aprovechando la propiedad NxN de la matriz
                
                if (($x + 1) < $length && ($y + 1) < $length) {
                    if ($dna[$x][$y] == $dna[$x + 1][$y + 1]) {
                        $diagonalPositiva[$y] += 1;
                    }
                }

                // se crean indices primo para facilitar el recorrido de la diagonal 
                // negativa, se recorre de forma inversa la matriz

                $xPrima = $length - $x - 1;
                $yPrima = $length - $y - 1;

                if ($xPrima < $length && $yPrima >= 0) {
                    if (($xPrima - 1) >= 0 && ($yPrima + 1) < $length) {
                        if ($dna[$xPrima][$yPrima] == $dna[$xPrima -1][$yPrima + 1]) {
                            $diagonalNegativa[$y] += 1;
                        }
                    }
                }
                
            }

            // terminando el recorrido sobre la linea se realiza el recorrido de los fifos
            $out = array_pop($diagonalPositiva);

            if ($out >= 3) {
                $conteo += intdiv($out,3);
            }
            
            $diagonalPositiva = array_merge([0],$diagonalPositiva);

            // diagonal negativa
            $out = array_pop($diagonalNegativa);
            if ($out >= 3) {
                $conteo += intdiv($out,3);
            }

            $diagonalNegativa = array_merge([0],$diagonalNegativa);
        }

        // conteo de los resultados sobre las lineas
        for ($i=0; $i < $length; $i++) { 
            if ($controlX[$i] >= 3) {
                $conteo += intdiv($controlX[$i],3);
            }
        }

        return $conteo;        
    }    
}
