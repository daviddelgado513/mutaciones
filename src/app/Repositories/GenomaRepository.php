<?php

namespace App\Repositories;

use App\Models\Genoma;

class GenomaRepository implements GenomaRepositoryInterface
{

    public function create($data)
    {
        // comprimir el genoma
        $len = count($data['dna']);
        $comprimido = array_merge($data['dna']);
        $comprimido = implode($comprimido);        

        // almacenar el genoma 

        $adn = Genoma::where('adn',$comprimido)->first();

        if ($adn == null) {
            $adn = Genoma::create([
                'adn' => $comprimido,
                'length' => $len,
                'mutado' => $data['mutado']
            ]);                
        }        
        
    }


    public function stats()
    {
        $mutados = count(Genoma::where('mutado',1)->get());
        $noMutados = count(Genoma::where('mutado',0)->get());
        $total = $mutados + $noMutados;

        return [
            'count_mutations' => $mutados,
            'count_no_mutation' => $noMutados,
            'ratio' => $mutados / $total
        ];
    }

}