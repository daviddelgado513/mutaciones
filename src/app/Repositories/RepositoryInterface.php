<?php

namespace App\Repositories;

/**
 * Clase repository general, si quisiera cambiar el comportamiento del
 * motor de almacenamiento en el futuro por otra base de datos en las 
 * dependency injection debo asegurarme que siempre injecten un repository
 * que derive de esta interfaz
 */
interface RepositoryInterface {
    
    public function create(array $data);
}