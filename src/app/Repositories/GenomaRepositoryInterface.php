<?php

namespace App\Repositories;

/**
 * Por contrato con la anterior interfaz debe implementar los metodos basicos
 * plus los que deriven en esta interfaz
 * 
 * Manipulacion de los registros de genomas mutados
 */
interface GenomaRepositoryInterface extends RepositoryInterface{

    public function stats();
    
}