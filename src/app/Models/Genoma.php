<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genoma extends Model
{
    use HasFactory;

    protected $fillable = [
        'adn',
        'length',
        'mutado'
    ];
}
